import {
  BelongsTo,
  Column,
  ForeignKey,
  Model,
  Table,
} from "sequelize-typescript";
import { Categories } from "./categories";

@Table
export class Gifs extends Model {
  @Column
  link!: string;

  @ForeignKey(() => Categories)
  @Column
  categoryId!: number;

  @BelongsTo(() => Categories)
  category!: Categories;
}
