import app from "./api/app";
import * as config from "../config.json";
import { Logger, LoggerType } from "./model/logger/logger";
import { sequelize } from "./model/database/database";

const port = config.API_PORT;

const server = app.listen(port, async () => {
  await sequelize.sync({ alter: false });

  const socketInfo = server.address();
  let startupMessage = "[HTTP-SERVER] Server started.";

  if (typeof socketInfo === "object" && socketInfo?.address) {
    if (socketInfo.address === "::") {
      startupMessage += ` http://localhost:${socketInfo.port}`;
    } else {
      startupMessage += ` http://${socketInfo.address}:${socketInfo.port}`;
    }
  } else if (typeof socketInfo === "string") {
    startupMessage += ` Pipe name or Unix domain socket name: ${socketInfo}`;
  }

  Logger.log(LoggerType.INFO, startupMessage);
});
