import { Sequelize } from "sequelize-typescript";
import * as config from "../../../config.json";
import { Categories } from "./tables/categories";
import { Gifs } from "./tables/gifs";

/**
 * Creation of the database
 * Most of the parameters are in the configuration file
 */
export const sequelize = new Sequelize(
  config.dbName,
  config.dbLogin,
  config.dbPassword,
  {
    host: "localhost",
    dialect: "sqlite",
    logging: false,
    // SQLite only
    storage: config.dbFile,
    models: [Gifs, Categories],
  }
);
