# gifs-manager-api

## Installation on server

### Setup the projet

On the server that should run the bot type :

```sh
apt install -y curl libssl-dev build-essential git sudo vim
adduser runner
adduser runner sudo
cd /home/runner
su runner
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
exit 
su runner
nvm install node // v16.13.2
npm install pm2 -g
git clone https://gitlab.com/BastienMarais-Public/gifs-manager.git
cd gifs-manager/back
npm i
```

It is necessary to create a file `./config.json` :

```json
{
    "API_PORT": 8081,
    "USAGE_PASSWORD": "MyBestPassword",
    "dbLogin": "gifs-manager-api",
    "dbPassword": "MyBestPassword2",
    "dbName": "gifs-manager-api",
    "dbFile": "database.db"
}
```

| Key            | description                                          |
| :------------- | :--------------------------------------------------- |
| API_PORT       | Port on which the api is listening                   |
| USAGE_PASSWORD | Password required for modification                   |
| dbLogin        | Database user name                                   |
| dbPassword     | Database user's password                             |
| dbName         | Name of the database                                 |
| dbFile         | Name of the database file for example `database.db`. |

### Launching the api

#### DEV

```sh
npm run dev
```

#### PROD

```sh
npm run preprod
pm2 start "node dist/src/index.js"
// OR
npm run prod
```