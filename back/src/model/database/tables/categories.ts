import { Column, HasMany, Model, Table } from "sequelize-typescript";
import { Gifs } from "./gifs";

@Table
export class Categories extends Model {
  @Column
  name!: string;

  @HasMany(() => Gifs)
  gifs!: Gifs[];
}
