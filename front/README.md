# gifs-manager-front

## Installation on server

### Setup the projet

On the server that should run the bot type :

```sh
apt install -y curl libssl-dev build-essential git sudo vim
adduser runner
adduser runner sudo
cd /home/runner
su runner
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
exit 
su runner
nvm install node // v16.13.2
npm install pm2 -g
git clone https://gitlab.com/BastienMarais-Public/gifs-manager.git
cd gifs-manager/front
npm i
npm install -g serve
```

It is necessary to create a file `./src/config.json` :

```json
{
    "API_URL": "https://...",
}
```

| Key     | description |
| :------ | :---------- |
| API_URL | Url to api  |


### Launching the api

#### DEV

```sh
npm run start
```

#### PROD

```sh
npm run build
pm2 start "serve -s build"
// OR
npm run start
```