import { Categories } from "../model/database/tables/categories";
import { Gifs } from "../model/database/tables/gifs";
import * as config from "../../config.json";

/**
 * @description Intermediate class between the model and the api
 * @class Backend
 */
class Backend {
  /**
   * Check that the entered password corresponds to the one in the configuration
   * @param password The password to test
   * @returns
   */
  checkUsagePassword(password: string): boolean {
    return config.USAGE_PASSWORD === password;
  }

  /**
   * Return the list of category names
   * @returns the list of category names
   */
  async getCategories(): Promise<string[]> {
    const categories: Categories[] =
      (await Categories.findAll({ order: [["name", "ASC"]] })) || [];
    const result: string[] = [];
    for (const cat of categories) {
      result.push(cat.name);
    }
    return result;
  }

  /**
   * Add a category to the database
   * @param name Name of the category
   * @returns The status message
   */
  async addCategory(name: string): Promise<string> {
    const dbCategory = await Categories.findOne({ where: { name: name } });
    if (!dbCategory) {
      await Categories.create({ name: name });
      return "OK";
    }
    return "A category with the same name exists";
  }

  /**
   * Remove a category to the database
   * @param name Name of the category
   * @returns The status message
   */
  async removeCategory(name: string): Promise<string> {
    const dbCategory = await Categories.findOne({ where: { name: name } });
    if (dbCategory) {
      const data = await Gifs.findAll({ where: { categoryId: dbCategory.id } });
      for (const gif of data) {
        await gif.destroy();
      }
      await dbCategory.destroy();
      return "OK";
    }
    return "No matching category found";
  }

  /**
   * Returns all gifs in a category
   * @param category Name of the category
   * @returns A list of links
   */
  async getAllGifsByCategory(category: string): Promise<string[]> {
    const dbCategory = await Categories.findOne({ where: { name: category } });
    const result: string[] = [];
    if (dbCategory) {
      const gifs = await Gifs.findAll({ where: { categoryId: dbCategory.id } });
      for (const gif of gifs) {
        result.push(gif.link);
      }
    }
    return result;
  }

  /**
   * Returns the link of a gif of the category
   * @param category Name of the category
   * @returns The link of the gif
   */
  async getGif(category: string): Promise<string> {
    const dbCategory = await Categories.findOne({ where: { name: category } });
    if (dbCategory) {
      const data = await Gifs.findAll({ where: { categoryId: dbCategory.id } });
      if (data && data.length > 0) {
        const index = Math.floor(Math.random() * data.length) || 0;
        return data[index]?.link || "";
      }
      return "There is no gif in this category";
    }
    return "The category doesn't exist";
  }

  /**
   * Add a gif into the database
   * @param category Name of the category
   * @param link Link of the gif
   * @returns The status message
   */
  async addGif(category: string, link: string): Promise<string> {
    if (link === "") return "You need a valid link";
    const dbCategory = await Categories.findOne({ where: { name: category } });
    if (dbCategory) {
      const dbGif = await Gifs.findOne({
        where: { categoryId: dbCategory.id, link: link },
      });
      if (!dbGif) {
        await Gifs.create({ link: link, categoryId: dbCategory.id });
        return "OK";
      }
      return "Gif already present";
    }
    return "The category doesn't exist";
  }

  /**
   * Delete a gif from the database
   * @param category Name of the category
   * @param link Link of the gif
   * @returns The status message
   */
  async removeGif(category: string, link: string): Promise<string> {
    const dbCategory = await Categories.findOne({ where: { name: category } });
    if (dbCategory) {
      const dbGif = await Gifs.findOne({
        where: { categoryId: dbCategory.id, link: link },
      });
      if (dbGif) {
        await dbGif.destroy();
        return "OK";
      }
      return "The gif doesn't exist";
    }
    return "The category doesn't exist";
  }
}

export default Backend;
