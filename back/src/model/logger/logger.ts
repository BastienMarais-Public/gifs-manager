import * as fs from "fs";
import * as chalk from "chalk";

/**
 * Type of logs available
 */
export const enum LoggerType {
  INFO = "INFO",
  WARN = "WARN",
  ERROR = "ERROR",
}

interface LoggerInterface {
  logsFolder: string;
  retention: number;
  log(type: LoggerType, message: string): void;
  clean(): void;
  getLoggerFile(date: Date): string;
}

/**
 * Allows you to generate logs and delete old logs
 */
export class CustomLogger implements LoggerInterface {
  logsFolder: string = "./logs";
  retention: number = 3;

  /**
   * CustomLogger constructor
   * @param logsFolder Relative path to the log folder
   * @param retention  Number of days of file retention
   */
  constructor(logsFolder: string, retention: number) {
    this.logsFolder = logsFolder;
    this.retention = retention;
  }

  /**
   * Allows you to write and format a log
   * @param type Type of log described in LoggerType
   * @param message Content of the log
   */
  log(type: LoggerType, message: string): void {
    const date: Date = new Date();
    const data: string = `${date.toISOString()} [${type}] ${message}`;

    fs.appendFile(this.getLoggerFile(date), data + "\n", (err) => {
      if (err) console.error(err);
    });

    switch (type) {
      case LoggerType.INFO:
        console.log(chalk.cyan(data));
        break;
      case LoggerType.WARN:
        console.log(chalk.yellow(data));
        break;
      case LoggerType.ERROR:
        console.log(chalk.red(data));
        break;
      default:
        console.log(chalk.magenta(data));
    }
  }

  /**
   * Deletes files that do not respect the retention rule
   */
  clean(): void {
    const logsFiles: string[] = fs
      .readdirSync(this.logsFolder)
      .filter((file) => file.endsWith(".log"));

    const retentionFiles: string[] = [];
    const date: Date = new Date();

    for (let i = 0; i < this.retention; i++) {
      retentionFiles.push(this.getLoggerFile(date));
      date.setDate(date.getDate() - 1);
    }

    for (const index in logsFiles) {
      const path: string = `${this.logsFolder}/${logsFiles[index]}`;
      if (retentionFiles.indexOf(path) !== -1) {
        this.log(LoggerType.INFO, `Service checkLogs > Kept ${path}`);
      } else {
        this.log(LoggerType.INFO, `Service checkLogs > Remove ${path}`);
        fs.unlink(path, (err) => {
          if (err)
            Logger.log(
              LoggerType.ERROR,
              `Service checkLogs > Error while deleting the file: ${err}`
            );
        });
      }
    }
  }

  /**
   * Generates the log file path for a given date
   * @param date Date of the file
   * @returns The file path for the given date
   */
  getLoggerFile(date: Date): string {
    let month = (date.getMonth() + 1).toString();
    if (month.length === 1) month = "0" + month;

    let day = date.getDate().toString();
    if (day.length === 1) day = "0" + day;

    return `${this.logsFolder}/${date.getFullYear()}_${month}_${day}.log`;
  }
}

const Logger: CustomLogger = new CustomLogger("./logs", 3);
export { Logger };
