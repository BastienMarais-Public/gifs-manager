import * as bodyParser from "body-parser";
import Backend from "../business/backend";
import { Logger, LoggerType } from "../model/logger/logger";
import * as express from "express";

const backend = new Backend();
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (_req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

/*********************************************************
 * GET
 ********************************************************/

app.get("/categories", async (req, res) => {
  const result = await backend.getCategories();
  Logger.log(LoggerType.INFO, `Get the list of categories [${result}]`);
  res.status(200).json(result);
});

app.get("/categories/:name", async (req, res) => {
  const { name } = req.params;
  const result = await backend.getAllGifsByCategory(name || "");
  Logger.log(
    LoggerType.INFO,
    `Get all gifs from ${name} category: [${result}]`
  );
  res.status(200).json(result);
});

app.get("/categories/:name/random", async (req, res) => {
  const { name } = req.params;
  const result = await backend.getGif(name || "");
  if (result.startsWith("http")) {
    Logger.log(
      LoggerType.INFO,
      `Get random gif from ${name} category: ${result}`
    );
    res.status(200).json(result);
  } else {
    Logger.log(
      LoggerType.WARN,
      `Get random gif from ${name} category: ${result}`
    );
    res.status(400).json(result);
  }
});

/*********************************************************
 * POST
 ********************************************************/
app.post("/categories/:name/add", async (req, res) => {
  const { link, usagePassword } = req.body;
  const { name } = req.params;
  if (backend.checkUsagePassword(usagePassword)) {
    const result = await backend.addGif(name, link || "");
    if (result === "OK") {
      res.status(200).json(result);
      Logger.log(
        LoggerType.INFO,
        `Add a gif in the category ${name}: ${link} => ${result}`
      );
    } else {
      res.status(400).json(result);
      Logger.log(
        LoggerType.WARN,
        `Add a gif in the category ${name}: ${link} => ${result}`
      );
    }
  } else {
    res.status(403).json("You need a password to use this command");
    Logger.log(
      LoggerType.ERROR,
      `You need a password to add a gif into category: [${name}, ${link}, ${usagePassword}]`
    );
  }
});

app.post("/categories/:name/", async (req, res) => {
  const { name } = req.params;
  const { usagePassword } = req.body;

  if (backend.checkUsagePassword(usagePassword)) {
    const result = await backend.addCategory(name);
    if (result === "OK") {
      res.status(200).json(result);
      Logger.log(LoggerType.INFO, `Add the ${name} category => ${result}`);
    } else {
      res.status(400).json(result);
      Logger.log(LoggerType.WARN, `Add the ${name} category => ${result}`);
    }
  } else {
    res.status(403).json("You need a password to use this command");
    Logger.log(
      LoggerType.ERROR,
      `You need a password to add a category: [${name}, ${usagePassword}]`
    );
  }
});

/*********************************************************
 * DELETE
 ********************************************************/
app.delete("/categories/:name/remove", async (req, res) => {
  const { link, usagePassword } = req.body;
  const { name } = req.params;

  if (backend.checkUsagePassword(usagePassword)) {
    const result = await backend.removeGif(name, link || "");
    if (result === "OK") {
      res.status(200).json(result);
      Logger.log(
        LoggerType.INFO,
        `Remove a gif in the category ${name}: ${link} => ${result}`
      );
    } else {
      res.status(400).json(result);
      Logger.log(
        LoggerType.WARN,
        `Remove a gif in the category ${name}: ${link} => ${result}`
      );
    }
  } else {
    res.status(403).json("You need a password to use this command");
    Logger.log(
      LoggerType.ERROR,
      `You need a password to remove a gif from category: [${name}, ${link}, ${usagePassword}]`
    );
  }
});

app.delete("/categories/:name/", async (req, res) => {
  const { name } = req.params;
  const { usagePassword } = req.body;

  if (backend.checkUsagePassword(usagePassword)) {
    const result = await backend.removeCategory(name);
    if (result === "OK") {
      res.status(200).json(result);
      Logger.log(LoggerType.INFO, `Remove the ${name} category => ${result}`);
    } else {
      res.status(400).json(result);
      Logger.log(LoggerType.WARN, `Remove the ${name} category => ${result}`);
    }
  } else {
    res.status(403).json("You need a password to use this command");
    Logger.log(
      LoggerType.ERROR,
      `You need a password to remove category: [${name}, ${usagePassword}]`
    );
  }
});

export default app;
